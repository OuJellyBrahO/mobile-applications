package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

import static com.example.kadern.samples2017.UserDetailsActivity.DB_STATE;
import static com.example.kadern.samples2017.UserDetailsActivity.USER_ID_EXTRA;

public class UserListActivity extends AppCompatActivity {

    AppClass app;
    ListView userListView;
    Button btnAddUser;
    ArrayList<User> users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        app = (AppClass)getApplication();


        userListView = (ListView)findViewById(R.id.userListView);
        btnAddUser = (Button)findViewById(R.id.btnAddUser);

        /*
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, app.users);

        userListView.setAdapter(adapter);

        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = app.users.get(position);
                //Toast.makeText(UserListActivity.this, selectedUser.getFirstName(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.USER_ID_EXTRA, position);
                startActivity(i);
            }
        });
        */


        users = app.userDa.getAllUsers();
        ArrayAdapter adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View listItemView = super.getView(position, convertView, parent);
                User currentUser = users.get(position);
                listItemView.setTag(currentUser);

                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                lbl.setText(currentUser.getFirstName());


                listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(USER_ID_EXTRA, selectedUser.getId());
                        i.putExtra(DB_STATE, "edit");
                        startActivity(i);
                    }
                });

                return listItemView;

            }

        };

        userListView.setAdapter(adapter);

        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.DB_STATE, "add");
                startActivity(i);

            }
        });

    }
}
