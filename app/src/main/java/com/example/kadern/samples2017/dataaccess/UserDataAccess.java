package com.example.kadern.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.kadern.samples2017.MySQLiteHelper;
import com.example.kadern.samples2017.MySQLiteOpenHelper;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by Perry on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    // We'll extend SQLiteHelper and customize it to create our database
    // We'll also use it to give us the SQLiteDatabase object that
    // represents our database
    private MySQLiteHelper dbHelper;

    // We should create static constants for the table name, and all column names
    public static final String TABLE_NAME = "user";
    public static final String COLUMN_USER_ID = "_id";
    public static final String  COLUMN_USER_NAME = "user_name";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_FAVORITE_MUSIC = "user_music";
    public static final String COLUMN_USER_ACTIVE = "user_active";

    // This is the sql statement to create the table (it will be used by dbHelper)
    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_NAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_USER_FAVORITE_MUSIC,
                    COLUMN_USER_ACTIVE
            );
    //Get an holder on the database
    private SQLiteDatabase database;


    //Get the database
    public UserDataAccess(MySQLiteOpenHelper dbHelper){
        // The dbHelper object will be used to get the object that represents our database
        this.database = dbHelper.getWritableDatabase();
    }
    //Inserts a user into the database
    public User insertUser(User i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, i.getFirstName());
        values.put(COLUMN_USER_EMAIL, i.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, i.getFavoriteMusic().ordinal());
        values.put(COLUMN_USER_ACTIVE, (i.isActive() ? 1 : 0));
        long insertId = database.insert(TABLE_NAME, null, values);
        // note: insertId will be -1 if the insert failed


        if(insertId != -1){
            i.setId(insertId);
            return i;
        }else{
            return null;
        }

    }
    //Gets all users for the database
    public ArrayList<User> getAllUsers(){

        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_NAME,COLUMN_USER_EMAIL,COLUMN_USER_FAVORITE_MUSIC,COLUMN_USER_ACTIVE, TABLE_NAME);

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){

            User i = new User(c.getLong(0), c.getString(1), c.getString(2), User.Music.values()[c.getInt(3)], (c.getInt(4) != 0 ));
            users.add(i);
            c.moveToNext(); // DONT FORGET THIS LINE!!!!!!

        }
        c.close();
        return users;

    }


    //Updates the user in a database
    public User updateUser(User i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, i.getId());
        values.put(COLUMN_USER_NAME, i.getFirstName());
        values.put(COLUMN_USER_EMAIL, i.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, i.getFavoriteMusic().ordinal());
        values.put(COLUMN_USER_ACTIVE, (i.isActive() ? 1: 0));

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + i.getId(), null);
        // this method returns the number of rows that were updated in the db
        // so that you could use it to confirm that your update worked


        if(rowsUpdated == 1){
            return i;
        }else{
            return null;
        }
    }
    //Delete a user from the database
    public int deleteUser(User i){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + i.getId(), null);
        // the above method returns the number of row that were deleted
        return rowsDeleted;
    }
}
