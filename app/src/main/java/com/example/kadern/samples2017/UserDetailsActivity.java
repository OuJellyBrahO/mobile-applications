package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";
    public static final String DB_STATE = "add";
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


    AppClass app;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;

    Button btnClearForm;
    Button btnInsertData;
    Button btnDeleteData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        app = (AppClass)getApplication();

        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);

        Intent i = getIntent();
        final String mode = i.getStringExtra(DB_STATE);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromUI();
                if(validate()){
                    if(mode.trim().equals("add")){
                        app.userDa.insertUser(user);
                    }
                    else if(mode.trim().equals("edit")){
                        app.userDa.updateUser(user);
                    }
                    Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                    startActivity(i);
                }


            }
        });

        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        btnInsertData = (Button)findViewById(R.id.btnInsertData);
        btnInsertData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mode.trim().equals("edit")){
                    putDataInUI();
                }
                else{
                    warning("No Data to Insert");
                }


            }
        });

        btnDeleteData = (Button)findViewById(R.id.btnDeleteData);
        btnDeleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteUser();
            }
        });


        long userId = i.getLongExtra(USER_ID_EXTRA,-1);
        Log.d("userId", ""+userId);
        if(userId >= 0){
            //Toast.makeText(this, "GET USER: " + userId, Toast.LENGTH_LONG).show();
            user = app.getUserById(userId);
            putDataInUI();
        }
    }

    private void getDataFromUI(){

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }


        if(user != null){
            user.setFirstName(firstName);
            user.setEmail(email);
            user.setActive(active);
            user.setFavoriteMusic(favoriteMusic);
        }else{
            user = new User(1,firstName,email,favoriteMusic,active);
        }

        //Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();

    }

    private  void putDataInUI(){

        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()){
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }

    private void deleteUser(){
        Intent i = getIntent();
        final String mode = i.getStringExtra(DB_STATE);
        if(mode.trim().equals("edit")){
            app.userDa.deleteUser(user);
            Intent j = new Intent(UserDetailsActivity.this, UserListActivity.class);
            startActivity(j);

        }
        else{
            warning("There is no user to delete");
        }
    }

    private void warning(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private boolean validate(){

        if(txtFirstName.getText().toString().trim().equals("")){
            warning("Please Enter A Name");
            return false;
        }
        if(txtEmail.getText().toString().trim().equals("")){
            warning("Please Enter an Email");
            return false;
        }
        if(!validateEmail(txtEmail.getText().toString())){
            warning("Please enter a valid email");
            return false;
        }
        if(rgFavoriteMusic.getCheckedRadioButtonId() == -1){
            warning("Please Select Favorite Music");
            return false;
        }




        return true;
    }


    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }
}
