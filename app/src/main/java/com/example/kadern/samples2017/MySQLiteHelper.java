package com.example.kadern.samples2017;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

// IMPORTANT - You will have to adjust these imports for your project
import com.example.kadern.samples2017.dataaccess.ItemDataAccess;


public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String TAG = "MySQLiteHelper";
    private static final String DATA_BASE_NAME = "users.db";
    private static final int DATABASE_VERSION = 1;

    // Note that when the constructor gets called, Android will check to see
    // if the database file (samples.db) has been created.
    // It will invoke onCreate to create database if it has not already been created
    // If the database has already been created, then Android will
    // look at the DATABASE_VERSION param
    // and compare it to the the version number in samples.db
    // If the param (DATABASE_VERSION) is greater than the version number
    // in samples.db, then Android will call onUpgrade
    public MySQLiteHelper(Context context){
        super(context, DATA_BASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create the 'items' table
        String sql = ItemDataAccess.TABLE_CREATE;
        db.execSQL(sql);
        Log.d(TAG, "creating db: " + sql);

        // if you needed other tables for your app, you would create them here as well
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // There is a strategy for upgrading the database, but it's complicated
        // if your app has already been distributed to users.
        // You have to be careful to upgrade the database so
        // that when users update your app, their data is not corrupted
    }
}
